﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EDMI.Models.Response
{
    public class BaseResponseRes
    {
        public bool Correct { get; set; } = true;
        public List<string> Errors { get; set; } = new List<string>();
    }
}
