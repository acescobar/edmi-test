﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EDMI.Models.Enums
{
    public enum States
    {
       OutOfService = 0,
       InUse = 1
    }
}
