﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EDMI.Models.Request
{
    public class AddElectricMeterReq
    {
        public int SerialNumber { get; set; }
        public string FirmwareVersion { get; set; }
        public string State { get; set; }
    }
}
