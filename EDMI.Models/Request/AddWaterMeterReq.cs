﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EDMI.Models.Request
{
    public class AddWaterMeterReq
    {
        public int SerialNumber { get; set; }
        public string FirmwareVersion { get; set; }
        public string State { get; set; }
    }
}
