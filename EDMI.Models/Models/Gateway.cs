﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Text;
using EDMI.Models.Enums;
namespace EDMI.Models.Models
{
    public class Gateway
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public string SerialNumber { get; set; }
        public string FirmwareVersion { get; set; }
        public States State { get; set; }
        public string IP { get; set; }
        public int Port { get; set; }
    }
}
