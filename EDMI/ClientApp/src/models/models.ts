export class Device {
  type: string;
  serialNumber: string;
  firmwareVersion: string;
  state: number;
  ip: string;
  port: number;
}

export class BaseResponseRes {
  correct: boolean;
  errors: string[];
}
