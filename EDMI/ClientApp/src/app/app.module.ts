import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { RegisterDeviceComponent } from './register-device/register-device.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogElementsExampleDialog } from './dialog/dialog-elements-example-dialog';

import {
  MatDialogModule,
  MatDialog
} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    RegisterDeviceComponent,
    DialogElementsExampleDialog
  ],
  imports: [
    CommonModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    ReactiveFormsModule,
    MatDialogModule,
    BrowserAnimationsModule,

    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'register-device', component: RegisterDeviceComponent },
      { path: 'dialog-elements-example-dialog', component: DialogElementsExampleDialog }
    ])
  ],
  providers: [MatDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
