import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Device, BaseResponseRes } from '../../models/models';
import {MatDialog} from '@angular/material';
import { DialogElementsExampleDialog } from '../dialog/dialog-elements-example-dialog';

@Component({
  selector: 'app-register-device',
  templateUrl: './register-device.component.html',
  styleUrls: ['./register-device.component.css']
})
export class RegisterDeviceComponent {

  device: Device = new Device();
  deviceForm;
  gatewayFieldsEnabled: boolean = false;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private formBuilder: FormBuilder,public dialog: MatDialog) {
    this.deviceForm = this.formBuilder.group({
      type: new FormControl(this.device.type, [Validators.required]),
      serialNumber: new FormControl(this.device.serialNumber, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(30),
      ]),
      firmwareVersion: new FormControl(this.device.firmwareVersion, [
        Validators.pattern('(\[0-9]+)\.(\[0-9]+)'),
        Validators.maxLength(20)
      ]),
      state: new FormControl(this.device.state),
      ip: new FormControl(this.device.ip, [
        Validators.pattern('(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)')
      ]),
      port: new FormControl(this.device.port, [
        Validators.min(0),
        Validators.max(65535),
      ]),
    });
  }

  enableGatewayFields() {
    this.gatewayFieldsEnabled = true;
  }

  disableGatewayFields() {
    this.gatewayFieldsEnabled = false;
  }

  onChangeType(value) {
    value == "Gateway" ? this.enableGatewayFields() : this.disableGatewayFields();
  }

  onSubmit(deviceData) {
    this.http.post<BaseResponseRes>(this.baseUrl + 'api/' + deviceData.type, deviceData).subscribe(result => {
      
      this.openDialog('Device registered!');
      this.deviceForm.reset();
    }, result => {
      
      this.openDialog('Errors:\n' + result.error.errors.join('\n'));
    });
  }

  openDialog(text): void {
    const dialogRef = this.dialog.open(DialogElementsExampleDialog , {
      width: '250px',
      data: { message: text }
    });
  }
}
