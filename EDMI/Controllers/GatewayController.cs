﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EDMI.Models.Models;
using EDMI.Models.Response;
using EDMI.Services.Interfaces;

namespace EDMI.Controllers
{
    [Route("api/[controller]")]
    public class GatewayController : Controller
    {
        IGatewayService _gatewayService;

        public GatewayController(IGatewayService gatewayService)
        {
            _gatewayService = gatewayService;
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var result = await _gatewayService.GetAll();
            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] Gateway gateway)
        {
            BaseResponseRes result;
            if (gateway == null)
            {
                return BadRequest();
            }

            result = await _gatewayService.Exists(gateway.SerialNumber);
            if (!result.Correct)
            {
                return BadRequest(result);
            }

            result = _gatewayService.Validate(gateway);
            if (!result.Correct)
            {
                return BadRequest(result);
            }

            result = await _gatewayService.Add(gateway);
            return Ok(result);
        }
    }
}
