﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EDMI.Models.Models;
using EDMI.Models.Response;
using EDMI.Services.Interfaces;

namespace EDMI.Controllers
{
    [Route("api/[controller]")]
    public class ElectricMeterController : Controller
    {
        IElectricMeterService _electricMeterService;

        public ElectricMeterController(IElectricMeterService electricMeterService)
        {
            _electricMeterService = electricMeterService;
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var result = await _electricMeterService.GetAll();
            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] ElectricMeter electricMeter)
        {
            BaseResponseRes result;
            if (electricMeter == null)
            {
                return BadRequest();
            }

            result = await _electricMeterService.Exists(electricMeter.SerialNumber);
            if (!result.Correct)
            {
                return BadRequest(result);
            }

            result = _electricMeterService.Validate(electricMeter);
            if (!result.Correct)
            {
                return BadRequest(result);
            }

            result = await _electricMeterService.Add(electricMeter);
            return Ok(result);
        }
    }
}
