﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EDMI.Models.Models;
using EDMI.Models.Response;
using EDMI.Services.Interfaces;

namespace EDMI.Controllers
{
    [Route("api/[controller]")]
    public class WaterMeterController : Controller
    {
        IWaterMeterService _waterMeterService;

        public WaterMeterController(IWaterMeterService waterMeterService)
        {
            _waterMeterService = waterMeterService;
        }

        [HttpGet]
        public async Task<ActionResult> GetAll()
        {
            var result = await _waterMeterService.GetAll();
            return Ok(result);
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody] WaterMeter waterMeter)
        {
            BaseResponseRes result;
            if (waterMeter == null)
            {
                return BadRequest();
            }

            result = await _waterMeterService.Exists(waterMeter.SerialNumber);
            if (!result.Correct)
            {
                return BadRequest(result);
            }

            result = _waterMeterService.Validate(waterMeter);
            if (!result.Correct)
            {
                return BadRequest(result);
            }

            result = await _waterMeterService.Add(waterMeter);
            return Ok(result);
        }
    }
}
