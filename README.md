# EDMI
EDMI Initial test for recruitment

# BBDD Instructions 

1- Open solution with Visual Studio

2- Create database executing EDMI_scrpit.sql

3- Edit appsettings.json files and change connection string

# Publish angular web app

1- Right-click on "EDMI" project and click Publish. At next screen click publish button

2- Copy content from publish folder to IIS folder

3- Open IIS folder from browser

# Publish Console app

1- Right-click on "EDMI.Console" project and click Publish. At next screen click publish button

2- Go to publish folder and execute "dotnet EDMI.Console.dll"
