﻿using System;
using System.Collections.Generic;
using System.Text;
using EDMI.Models.Models;
using System.Threading.Tasks;


namespace EDMI.Data.Interfaces
{
    public interface IWaterMeterRepository
    {
        Task<bool> Add(WaterMeter watermeter);
        Task<List<WaterMeter>> GetAll();
        Task<bool> Exists(string serialNumber);
    }
}
