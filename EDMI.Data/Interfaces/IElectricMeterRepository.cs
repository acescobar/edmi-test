﻿using System;
using System.Collections.Generic;
using System.Text;
using EDMI.Models.Models;
using System.Threading.Tasks;

namespace EDMI.Data.Interfaces
{
    public interface IElectricMeterRepository
    {
        Task<bool> Add(ElectricMeter electricmeter);
        Task<List<ElectricMeter>> GetAll();
        Task<bool> Exists(string serialNumber);
    }
}
