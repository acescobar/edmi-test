﻿using System;
using System.Collections.Generic;
using System.Text;
using EDMI.Models.Models;
using System.Threading.Tasks;

namespace EDMI.Data.Interfaces
{
    public interface IGatewayRepository
    {
        Task<bool> Add(Gateway gateway);
        Task<List<Gateway>> GetAll();
        Task<bool> Exists(string serialNumber);
    }
}
