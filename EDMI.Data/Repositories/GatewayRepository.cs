﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EDMI.Data.Interfaces;
using EDMI.Models.Models;
using Microsoft.EntityFrameworkCore;

namespace EDMI.Data.Repositories
{
    public class GatewayRepository : IGatewayRepository
    {
        private readonly EdmiDbContext _dbEdmi;

        public GatewayRepository(EdmiDbContext dbEdmi)
        {
            _dbEdmi = dbEdmi;
        }

        public async Task<bool> Add(Gateway gateway)
        {
            await _dbEdmi.Gateways.AddAsync(gateway);
            await _dbEdmi.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Exists(string serialNumber)
        {
            return await _dbEdmi.Gateways.AnyAsync(x => x.SerialNumber == serialNumber);
        }

        public async Task<List<Gateway>> GetAll()
        {
            return await _dbEdmi.Gateways.ToListAsync();
        }
    }
}
