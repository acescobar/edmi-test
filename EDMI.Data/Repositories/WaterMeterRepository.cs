﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EDMI.Data.Interfaces;
using EDMI.Models.Models;
using Microsoft.EntityFrameworkCore;

namespace EDMI.Data.Repositories
{
    public class WaterMeterRepository : IWaterMeterRepository
    {
        private readonly EdmiDbContext _dbEdmi;

        public WaterMeterRepository(EdmiDbContext dbEdmi)
        {
            _dbEdmi = dbEdmi;
        }

        public async Task<bool> Add(WaterMeter watermeter)
        {
            await _dbEdmi.WaterMeters.AddAsync(watermeter);
            await _dbEdmi.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Exists(string serialNumber)
        {
            return await _dbEdmi.WaterMeters.AnyAsync(x => x.SerialNumber == serialNumber);
        }

        public async Task<List<WaterMeter>> GetAll()
        {
            return await _dbEdmi.WaterMeters.ToListAsync();
        }
    }
}
