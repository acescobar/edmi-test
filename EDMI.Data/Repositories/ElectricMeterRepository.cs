﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using EDMI.Data.Interfaces;
using EDMI.Models.Models;
using Microsoft.EntityFrameworkCore;


namespace EDMI.Data.Repositories
{
    public class ElectricMeterRepository : IElectricMeterRepository
    {
        private readonly EdmiDbContext _dbEdmi;

        public ElectricMeterRepository(EdmiDbContext dbEdmi)
        {
            _dbEdmi = dbEdmi;
        }

        public async Task<bool> Add(ElectricMeter electricMeter)
        {
            await _dbEdmi.ElectricMeters.AddAsync(electricMeter);
            await _dbEdmi.SaveChangesAsync();
            return true;
        }

        public async Task<bool> Exists(string serialNumber)
        {
            return await _dbEdmi.ElectricMeters.AnyAsync(x => x.SerialNumber == serialNumber);
        }

        public async Task<List<ElectricMeter>> GetAll()
        {
            return await _dbEdmi.ElectricMeters.ToListAsync();
        }
    }
}
