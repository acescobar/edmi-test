﻿using EDMI.Data.Interfaces;
using EDMI.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace EDMI.Data
{
    public static class RegisterDataService
    {
        public static IServiceCollection AddCustomDataServices(this IServiceCollection services)
        {
            services.AddScoped<IElectricMeterRepository, ElectricMeterRepository>();
            services.AddScoped<IWaterMeterRepository, WaterMeterRepository>();
            services.AddScoped<IGatewayRepository, GatewayRepository>();

            return services;
        }
    }
}
