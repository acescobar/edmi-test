using EDMI.Data;
using EDMI.Data.Interfaces;
using EDMI.Models.Models;
using EDMI.Models.Enums;
using EDMI.Models.Response;
using EDMI.Services.Interfaces;
using EDMI.Services.Services;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using EDMI.Data.Repositories;

namespace Tests
{
    public class GatewayServiceTest
    {
        private ServiceProvider serviceProvider { get; set; }

        [SetUp]
        public void Setup()
        {
            var services = new ServiceCollection();
            services.AddTransient<IGatewayService, GatewayService>();
            services.AddTransient<IGatewayRepository, GatewayRepository>();
            services.AddDbContext<EdmiDbContext>();
            serviceProvider = services.BuildServiceProvider();

        }

        [Test]
        public void Validate_IP_Format_ReturnFalse()
        {
            var service = serviceProvider.GetService<IGatewayService>();
            var result = service.Validate(new Gateway()
            {
                SerialNumber = "12345678",
                FirmwareVersion = "1.5",
                State = States.InUse,
                IP="192.123.234.230.34",
                Port=5678

            });
            Assert.False(result.Correct, "Invalid IP format");
        }

        [Test]
        public void Validate_SerialNumberNull_ReturnFalse()
        {
            var service = serviceProvider.GetService<IGatewayService>();
            var result = service.Validate(new Gateway());
            Assert.False(result.Correct, "Serial number shoul not be null");
        }
    }
}