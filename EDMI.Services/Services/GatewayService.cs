﻿using System;
using System.Collections.Generic;
using System.Text;
using EDMI.Services.Interfaces;
using EDMI.Services.Services;
using EDMI.Models.Models;
using EDMI.Models.Enums;
using EDMI.Models.Response;
using EDMI.Data.Interfaces;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace EDMI.Services.Services
{
    public class GatewayService :IGatewayService
    {
        private IGatewayRepository _gatewayRepository;

        public GatewayService(IGatewayRepository gatewayRepository)
        {
            _gatewayRepository = gatewayRepository;
        }

        public async Task<BaseResponseRes> Add(Gateway gateway)
        {
            try
            {
                gateway.SerialNumber = "G" + gateway.SerialNumber;
                await _gatewayRepository.Add(gateway);
                return new BaseResponseRes { Correct = true };
            }
            catch (Exception ex)
            {
                return new BaseResponseRes { Correct = false, Errors = new List<string> { ex.Message } };
            }
        }

        public async Task<BaseResponseRes> Exists(string serialNumber)
        {
            BaseResponseRes result = new BaseResponseRes();
            bool exists = await _gatewayRepository.Exists("G" + serialNumber);
            if (exists)
            {
                result.Correct = false;
                result.Errors = new List<string> { "Serial Number exists" };
            }
            return result;
        }

        public async Task<List<Gateway>> GetAll()
        {
            return await _gatewayRepository.GetAll();
        }

        public BaseResponseRes Validate(Gateway gateway)
        {
            BaseResponseRes result = new BaseResponseRes();

            // Serial number
            if (string.IsNullOrEmpty(gateway.SerialNumber)) result.Errors.Add("Serial Number is required");
            if (!string.IsNullOrEmpty(gateway.SerialNumber) && gateway.SerialNumber.Length < 8) result.Errors.Add("Serial Number min length is 8");
            if (!string.IsNullOrEmpty(gateway.SerialNumber) && gateway.SerialNumber.Length > 30) result.Errors.Add("Serial Number max length is 30");

            // State
            if (!Enum.IsDefined(typeof(States), gateway.State)) result.Errors.Add("State value not recognized");

            // Firmware Version
            if (!string.IsNullOrEmpty(gateway.FirmwareVersion) && !new Regex(@"^(\d+\.){1}(\d+)$").IsMatch(gateway.FirmwareVersion)) result.Errors.Add("Invalid Firmware format (xx.xx)");

            // IP
            if (!string.IsNullOrEmpty(gateway.IP) && !new Regex(@"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$").IsMatch(gateway.IP)) result.Errors.Add("Invalid IP format (xxx.xxx.xxx.xxx)");

            // Port
            if (gateway.Port <= 0) result.Errors.Add("Port value must be greater than 0");
            if (gateway.Port > 65535) result.Errors.Add("Port value must be lower than 65535");

            result.Correct = result.Errors.Count == 0;
            return result;
        }
    }
}
