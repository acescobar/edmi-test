﻿using System;
using System.Collections.Generic;
using System.Text;
using EDMI.Services.Interfaces;
using EDMI.Services.Services;
using EDMI.Models.Models;
using EDMI.Models.Enums;
using EDMI.Models.Response;
using EDMI.Data.Interfaces;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace EDMI.Services.Services
{
    public class ElectricMeterService: IElectricMeterService
    {
        private IElectricMeterRepository _electricMeterRepository;

        public ElectricMeterService(IElectricMeterRepository electricMeterRepository)
        {
            _electricMeterRepository = electricMeterRepository;
        }

        public async Task<BaseResponseRes> Add(ElectricMeter electricMeter)
        {
            try
            {
                electricMeter.SerialNumber = "E" + electricMeter.SerialNumber;
                await _electricMeterRepository.Add(electricMeter);
                return new BaseResponseRes { Correct = true };
            }
            catch (Exception ex)
            {
                return new BaseResponseRes { Correct = false, Errors = new List<string> { ex.Message } };
            }
        }

        public async Task<BaseResponseRes> Exists(string serialNumber)
        {
            BaseResponseRes result = new BaseResponseRes();
            bool exists = await _electricMeterRepository.Exists("E" + serialNumber);
            if (exists)
            {
                result.Correct= false;
                result.Errors = new List<string> { "Serial Number exists" };
            }
            return result;
        }

        public async Task<List<ElectricMeter>> GetAll()
        {
            return await _electricMeterRepository.GetAll();
        }

        public BaseResponseRes Validate(ElectricMeter electricMeter)
        {
            BaseResponseRes result = new BaseResponseRes();

            // Serial number
            if (string.IsNullOrEmpty(electricMeter.SerialNumber)) result.Errors.Add("Serial Number is required");
            if (!string.IsNullOrEmpty(electricMeter.SerialNumber) && electricMeter.SerialNumber.Length < 8) result.Errors.Add("Serial Number min length is 8");
            if (!string.IsNullOrEmpty(electricMeter.SerialNumber) && electricMeter.SerialNumber.Length > 30) result.Errors.Add("Serial Number max length is 30");

            // State
            if (!Enum.IsDefined(typeof(States), electricMeter.State)) result.Errors.Add("State value not recognized");

            // Firmware Version
            if (!string.IsNullOrEmpty(electricMeter.FirmwareVersion) && !new Regex(@"^(\d+\.){1}(\d+)$").IsMatch(electricMeter.FirmwareVersion)) result.Errors.Add("Invalid Firmware format (xx.xx)");

            result.Correct = result.Errors.Count == 0;
            return result;
        }
    }
}
