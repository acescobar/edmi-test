﻿using System;
using System.Collections.Generic;
using System.Text;
using EDMI.Services.Interfaces;
using EDMI.Services.Services;
using EDMI.Models.Models;
using EDMI.Models.Enums;
using EDMI.Models.Response;
using EDMI.Data.Interfaces;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace EDMI.Services.Services
{
    public class WaterMeterService : IWaterMeterService
    {
        private IWaterMeterRepository _waterMeterRepository;

        public WaterMeterService(IWaterMeterRepository waterMeterRepository)
        {
            _waterMeterRepository = waterMeterRepository;
        }

        public async Task<BaseResponseRes> Add(WaterMeter waterMeter)
        {
            try
            {
                waterMeter.SerialNumber = "W" + waterMeter.SerialNumber;
                await _waterMeterRepository.Add(waterMeter);
                return new BaseResponseRes { Correct = true };
            }
            catch (Exception ex)
            {
                return new BaseResponseRes { Correct = false, Errors = new List<string> { ex.Message } };
            }
        }

        public async Task<BaseResponseRes> Exists(string serialNumber)
        {
            BaseResponseRes result = new BaseResponseRes();
            bool exists = await _waterMeterRepository.Exists("W" + serialNumber);
            if (exists)
            {
                result.Correct = false;
                result.Errors = new List<string> { "Serial Number exists" };
            }
            return result;
        }

        public async Task<List<WaterMeter>> GetAll()
        {
            return await _waterMeterRepository.GetAll();
        }

        public BaseResponseRes Validate(WaterMeter waterMeter)
        {
            BaseResponseRes result = new BaseResponseRes();

            // Serial number
            if (string.IsNullOrEmpty(waterMeter.SerialNumber)) result.Errors.Add("Serial Number is required");
            if (!string.IsNullOrEmpty(waterMeter.SerialNumber) && waterMeter.SerialNumber.Length < 8) result.Errors.Add("Serial Number min length is 8");
            if (!string.IsNullOrEmpty(waterMeter.SerialNumber) && waterMeter.SerialNumber.Length > 30) result.Errors.Add("Serial Number max length is 30");

            // State
            if (!Enum.IsDefined(typeof(States), waterMeter.State)) result.Errors.Add("State value not recognized");

            // Firmware Version
            if (!string.IsNullOrEmpty(waterMeter.FirmwareVersion) && !new Regex(@"^(\d+\.){1}(\d+)$").IsMatch(waterMeter.FirmwareVersion)) result.Errors.Add("Invalid Firmware format (xx.xx)");

            result.Correct = result.Errors.Count == 0;
            return result;
        }
    }
}
