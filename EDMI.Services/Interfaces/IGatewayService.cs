﻿using System;
using System.Collections.Generic;
using System.Text;
using EDMI.Models.Models;
using EDMI.Models.Response;
using System.Threading.Tasks;

namespace EDMI.Services.Interfaces
{
    public interface IGatewayService
    {
        Task<BaseResponseRes> Add(Gateway gateway);
        Task<BaseResponseRes> Exists(string serialNumber);
        BaseResponseRes Validate(Gateway gateway);
        Task<List<Gateway>> GetAll();
    }
}
