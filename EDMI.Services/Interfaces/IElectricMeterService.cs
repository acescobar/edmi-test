﻿using System;
using System.Collections.Generic;
using System.Text;
using EDMI.Models.Models;
using EDMI.Models.Response;
using System.Threading.Tasks;

namespace EDMI.Services.Interfaces
{
    public interface IElectricMeterService
    {
        Task<BaseResponseRes> Add(ElectricMeter electricmeter);
        Task<BaseResponseRes> Exists(string serialNumber);
        BaseResponseRes Validate(ElectricMeter electricmeter);
        Task<List<ElectricMeter>> GetAll();
    }
}
