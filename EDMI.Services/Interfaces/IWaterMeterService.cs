﻿using System;
using System.Collections.Generic;
using System.Text;
using EDMI.Models.Models;
using EDMI.Models.Response;
using System.Threading.Tasks;

namespace EDMI.Services.Interfaces
{
    public interface IWaterMeterService
    {
        Task<BaseResponseRes> Add(WaterMeter watermeter);
        Task<BaseResponseRes> Exists(string serialNumber);
        BaseResponseRes Validate(WaterMeter watermeter);
        Task<List<WaterMeter>> GetAll();
    }
}
