﻿using System;
using System.Collections.Generic;
using System.Text;
using EDMI.Services.Interfaces;
using EDMI.Services.Services;
using Microsoft.Extensions.DependencyInjection;

namespace EDMI.Services.Extensions
{
    public static class RegisterService
    {
        public static IServiceCollection AddCustomServices(this IServiceCollection services)
        {

            //Own Services
            services.AddScoped<IElectricMeterService, ElectricMeterService>();
            services.AddScoped<IWaterMeterService, WaterMeterService>();
            services.AddScoped<IGatewayService, GatewayService>();

            return services;
        }

    }
}
