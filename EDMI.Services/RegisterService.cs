﻿using EDMI.Services.Interfaces;
using EDMI.Services.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace EDMI.Services
{
    public static class RegisterService
    {
        public static IServiceCollection AddCustomServices(this IServiceCollection services)
        {
            services.AddScoped<IElectricMeterService, ElectricMeterService>();
            services.AddScoped<IWaterMeterService, WaterMeterService>();
            services.AddScoped<IGatewayService, GatewayService>();

            return services;
        }
    }
}
