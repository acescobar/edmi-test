﻿using EDMI.Data;
using EDMI.Services.Interfaces;
using EDMI.Services;
using EDMI.Models.Models;
using EDMI.Models.Response;
using EDMI.Models.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;


namespace EDMI.Console
{
    class Program
    {
        private static IServiceCollection servicesCollection;
        public static IConfiguration configuration { get; set; }
        public static ServiceProvider serviceProvider { get; set; }

        public static string ActualEnvironment { get { return Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"; } }

        static void Main(string[] args)
        {
            configuration = new ConfigurationBuilder()
                    .AddJsonFile($"appsettings.{ActualEnvironment}.json", false, true)
                    .Build();

            servicesCollection = new ServiceCollection();
            servicesCollection.AddDbContext<EdmiDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("EdmiDb")));

            servicesCollection.AddCustomServices().AddCustomDataServices();

            serviceProvider = servicesCollection.BuildServiceProvider();

            MainAsync().GetAwaiter().GetResult();
        }

        private static async Task MainAsync()
        {
            System.Console.WriteLine("Recruiter test console app:\n");

            string input = "";

            while (input != "exit" && input != "e" && input != "E")
            {
                switch (input)
                {
                    case "list": await ListAll(); break;
                    case "l": await ListAll(); break;
                    case "L": await ListAll(); break;
                    case "register": await RegisterOne(); break;
                    case "r": await RegisterOne(); break;
                    case "R": await RegisterOne(); break;
                    default: break;
                }
                ShowOptions();
                input = System.Console.ReadLine();
            }

        }

        private static void ShowOptions()
        {
            System.Console.WriteLine("Enter option:");
            System.Console.WriteLine("- list(l) -> List registered devices");
            System.Console.WriteLine("- regiter(r) -> Register a new device");
            System.Console.WriteLine("- exit(e)-> Exit\n");
        }

        private async static Task<bool> ListAll()
        {
            IElectricMeterService electricMeterService = serviceProvider.GetService<IElectricMeterService>();
            IWaterMeterService waterMeterService = serviceProvider.GetService<IWaterMeterService>();
            IGatewayService gatewayService = serviceProvider.GetService<IGatewayService>();

            
            int tableWidth = 120;

            System.Console.Clear();
            PrintLine(tableWidth);
            string[] cabecera= { "REGISTERED DEVICES"};
            PrintRow(cabecera,tableWidth);
            PrintLine(tableWidth);
            string[] columns = { "TYPE", "SERIAL NUMBER", "FIRMWARE VERSION", "STATE", "IP", "PORT" };
            PrintRow(columns,tableWidth);
            PrintLine(tableWidth);
            
       

            List<ElectricMeter> electricMeters = await electricMeterService.GetAll();
            if (electricMeters.Count > 0)
            {
                foreach (var item in electricMeters)
                {
                    PrintLine(tableWidth);
                    string[] ecolumns = { "Electric meter", item.SerialNumber, item.FirmwareVersion, PrintState((int)item.State), "", "" };
                    PrintRow(ecolumns,tableWidth);
                }
            }
            List<WaterMeter> waterMeters = await waterMeterService.GetAll();
            if (waterMeters.Count > 0)
            {
                

                foreach (var item in waterMeters)
                {
                    PrintLine(tableWidth);
                    string[] wcolumns = { "Water meter", item.SerialNumber, item.FirmwareVersion, PrintState((int)item.State), "", "" };
                    PrintRow(wcolumns, tableWidth);
                }
            }

            List<Gateway> gateways = await gatewayService.GetAll();
            if (gateways.Count > 0)
            {
               

                foreach (var item in gateways)
                {
                    PrintLine(tableWidth);
                    string[] gcolumns = { "Gateway", item.SerialNumber, item.FirmwareVersion, PrintState((int)item.State), item.IP,item.Port.ToString() };
                    PrintRow(gcolumns, tableWidth);
                }
               
            }
            PrintLine(tableWidth);
            System.Console.ReadLine();

            return true;
        }
        private static string PrintState(int state)
        {
            return state == 0 ? "Out of Service" : state == 1 ? "In Use" : "";
        }

        private async static Task<bool> RegisterOne()
        {
            IWaterMeterService waterMeterService = serviceProvider.GetService<IWaterMeterService>();
            IGatewayService gatewayService = serviceProvider.GetService<IGatewayService>();

            string type = "";
            string serialNumber;
            string firmwareVersion;
            string state = "";

            while (type != "E" && type != "W" && type != "G")
            {
                System.Console.Write("Device type (E)lectricMeter, (W)aterMeter, (G)ateway: ");
                type = System.Console.ReadLine().ToUpper();
            }

            System.Console.Write("Serial Number: ");
            serialNumber = System.Console.ReadLine();

            System.Console.Write("Firmware Version: ");
            firmwareVersion = System.Console.ReadLine();

            while (state != "0" && state != "1")
            {
                System.Console.Write("State (0)-Out of service, (1)-In use: ");
                state = System.Console.ReadLine();
            }

            BaseResponseRes response = new BaseResponseRes();
            switch (type)
            {
                case "E":

                    response = await AddElectricMeter(serialNumber, firmwareVersion, state);

                    System.Console.WriteLine();

                    if (response.Correct)
                    {
                        System.Console.WriteLine("Electric meter Registered successfully...");
                    }
                    else
                    {
                        System.Console.WriteLine("Errors occurred...");
                        foreach (string msg in response.Errors)
                        {
                            System.Console.WriteLine(msg);
                        }
                    }
                    break;
                case "W":
                    response = await AddWaterMeter(serialNumber, firmwareVersion, state);

                    System.Console.WriteLine();

                    if (response.Correct)
                    {
                        System.Console.WriteLine("Water meter Registered successfully...");
                    }
                    else
                    {
                        System.Console.WriteLine("Errors occurred...");
                        foreach (string msg in response.Errors)
                        {
                            System.Console.WriteLine(msg);
                        }
                    }
                    break;
                case "G":
                    string ip;
                    string port;

                    System.Console.Write("IP: ");
                    ip = System.Console.ReadLine();

                    System.Console.Write("Port: ");
                    port = System.Console.ReadLine();
                    response = await AddGateway(serialNumber, firmwareVersion, state, ip, port);

                    System.Console.WriteLine();

                    if (response.Correct)
                    {
                        System.Console.WriteLine("Gateway Registered successfully...");
                    }
                    else
                    {
                        System.Console.WriteLine("Errors occurred...");
                        foreach (string msg in response.Errors)
                        {
                            System.Console.WriteLine(msg);
                        }
                    }
                    break;
            }


            System.Console.WriteLine("");

            return true;
        }

        private async static Task<BaseResponseRes> AddElectricMeter(string serialNumber, string firmwareVersion, string state)
        {
            IElectricMeterService electricMeterService = serviceProvider.GetService<IElectricMeterService>();
            ElectricMeter electricMeter = new ElectricMeter
            {
                SerialNumber = serialNumber,
                FirmwareVersion = firmwareVersion,
                State = (States)Enum.Parse(typeof(States), state)
            };

            BaseResponseRes result = await electricMeterService.Exists(electricMeter.SerialNumber);
            if (!result.Correct)
            {
                return result;
            }

            result = electricMeterService.Validate(electricMeter);
            if (!result.Correct)
            {
                return result;
            }

            return await electricMeterService.Add(electricMeter);
        }

        private async static Task<BaseResponseRes> AddWaterMeter(string serialNumber, string firmwareVersion, string state)
        {
    
            IWaterMeterService waterMeterService = serviceProvider.GetService<IWaterMeterService>();
            WaterMeter waterMeter = new WaterMeter
            {
                SerialNumber = serialNumber,
                FirmwareVersion = firmwareVersion,
                State = (States)Enum.Parse(typeof(States), state)
            };

            BaseResponseRes result = await waterMeterService.Exists(waterMeter.SerialNumber);
            if (!result.Correct)
            {
                return result;
            }

            result = waterMeterService.Validate(waterMeter);
            if (!result.Correct)
            {
                return result;
            }

            return await waterMeterService.Add(waterMeter);
        }

        private async static Task<BaseResponseRes> AddGateway(string serialNumber, string firmwareVersion, string state, string ip, string port)
        {

            IGatewayService gatewayService = serviceProvider.GetService<IGatewayService>();
            int portNum;

            Gateway gateway = new Gateway
            {
                SerialNumber = serialNumber,
                FirmwareVersion = firmwareVersion,
                State = (States)Enum.Parse(typeof(States), state),
                IP = ip,
                Port = Int32.TryParse(port, out portNum) ? portNum : 0
            };

            BaseResponseRes result = await gatewayService.Exists(gateway.SerialNumber);
            if (!result.Correct)
            {
                return result;
            }

            result = gatewayService.Validate(gateway);
            if (!result.Correct)
            {
                return result;
            }

            return await gatewayService.Add(gateway);
        }

        //Paint table
        static void PrintLine(int tableWidth)
        {
            System.Console.WriteLine(new string('-', tableWidth));
        }

        static void PrintRow(string[] columns,int tableWidth)
        {
            int width = (tableWidth - columns.Length) / columns.Length;
            string row = "|";

            foreach (string column in columns)
            {
                row += AlignCentre(column, width) + "|";
            }

            System.Console.WriteLine(row);
        }

        static string AlignCentre(string text, int width)
        {
            text = text.Length > width ? text.Substring(0, width - 3) + "..." : text;

            if (string.IsNullOrEmpty(text))
            {
                return new string(' ', width);
            }
            else
            {
                return text.PadRight(width - (width - text.Length) / 2).PadLeft(width);
            }
        }

    }
}
