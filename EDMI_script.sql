-- script for creating tables for the Edmi app

CREATE TABLE [dbo].[ElectricMeters] (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [SerialNumber]    NVARCHAR (31)    NOT NULL,
    [FirmwareVersion] NVARCHAR (20)    NULL,
    [State]           INT              NULL
    CONSTRAINT PK_ElectricMeter PRIMARY KEY ([Id] ASC),
    CONSTRAINT U_ElectricMeter UNIQUE ([SerialNumber] ASC)
);

CREATE TABLE [dbo].[WaterMeters] (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [SerialNumber]    NVARCHAR (31)    NOT NULL,
    [FirmwareVersion] NVARCHAR (20)    NULL,
    [State]           INT              NULL
    CONSTRAINT PK_WaterMeter PRIMARY KEY ([Id] ASC),
    CONSTRAINT U_WaterMeter UNIQUE ([SerialNumber] ASC)
);

CREATE TABLE [dbo].[Gateways] (
    [Id]              UNIQUEIDENTIFIER NOT NULL,
    [SerialNumber]    NVARCHAR (31)    NOT NULL,
    [FirmwareVersion] NVARCHAR (20)    NULL,
    [State]           INT              NULL,
    [IP]              NVARCHAR(20)     NULL,
    [Port]            INT              NULL,
    CONSTRAINT PK_Gateway PRIMARY KEY ([Id] ASC),
    CONSTRAINT U_Gateway UNIQUE ([SerialNumber] ASC)
);
